#!/usr/bin/env python
#
# Multipoles_ml class. Predict multipole parameters from ML.
#
# Tristan Bereau (2017)

from electrostatics import Electrostatics
from system import System
import scipy
from scipy import stats
from scipy.spatial.distance import pdist, cdist, squareform
import numpy as np
import logging
import pickle
import constants
import math
import utils
import os

# Set logger
logger = logging.getLogger(__name__)

class MultipoleML(Electrostatics):
    'MultipoleML class. Predicts multipoles from machine learning.'

    def __init__(self, _calculator):
        Electrostatics.__init__(self, _calculator)
        atoms = [i for i in constants.atomic_weight.keys()]
        self.descr_train = {ele: [] for ele in atoms}
        self.descr_bob_train = {ele: [] for ele in atoms}
        self.target_vor_train = {ele: [] for ele in atoms}
        self.target_train = {ele: [] for ele in atoms}
        self.num_at_typ_train = {ele: 0 for ele in atoms}
        self.at_typ_train = {ele: [] for ele in atoms}
        self.at_typ_pred = []
        self.max_neighbors = self.calculator.Config.getint( \
            "multipoles","max_neighbors")
        # kernel ridge regression
        self.alpha_train = {}
        self.krr_kernel = self.calculator.Config.get( \
            "multipoles","krr_kernel")
        self.krr_sigma = self.calculator.Config.getfloat( \
            "multipoles","krr_sigma")
        self.krr_zeta = self.calculator.Config.getfloat( \
            "multipoles","krr_zeta")
        self.krr_beta = self.calculator.Config.getfloat( \
            "multipoles","krr_beta")
        self.krr_lambda = self.calculator.Config.getfloat( \
            "multipoles","krr_lambda")
        # support vector regression
        self.clf = None
        logger.setLevel(self.calculator.get_logger_level())
        self.bob_struct = None
        self.initialize_bag_of_bonds_struct()
        self.normalizer = None

    def load_ml(self, load_file=None):
        '''Load machine learning model for each atom'''
        # Try many atoms and see which atoms we find
        if load_file != None:
            logger.info(
                    "Reading multipole training from %s" % load_file)
            with open(load_file, 'rb') as f:
                    descr_train_at, descr_bob_train_at, target_vor_train_at, \
                        alpha_train, num_at_typ, at_typ_train = pickle.load(f)
            self.descr_train = descr_train_at
            self.descr_bob_train = descr_bob_train_at
            self.target_vor_train = target_vor_train_at
            self.alpha_train = alpha_train
            self.num_at_typ_train.update(num_at_typ)
            self.at_typ_train = at_typ_train
        else:
            atoms = [i for i in constants.atomic_weight.keys()]
            for a in atoms:
                if self.calculator.Config.has_option("multipoles",str("training_"+a)):
                    training_file = self.calculator.Config.get(
                        "multipoles",str("training_"+a))
                    logger.info(
                        "Reading multipole training from %s" % training_file)
                    with open(training_file, 'rb') as f:
                        descr_train_at, descr_bob_train_at, target_vor_train_at, \
                            alpha_train, num_at_typ, at_typ_train = pickle.load(f)
                    self.descr_train[a] = descr_train_at
                    self.descr_bob_train[a] = descr_bob_train_at
                    self.target_vor_train[a] = target_vor_train_at
                    self.alpha_train[a] = alpha_train
                    self.num_at_typ_train.update(num_at_typ)
                    self.at_typ_train[a] = at_typ_train
        return None

    def save_ml(self, save_file):
        '''Save machine learning model for each atom'''
        logger.info("Saving multipole machine learning model to %s" %
            save_file)
        with open(save_file, 'w') as f:
            pickle.dump([self.descr_train, self.descr_bob_train,
                self.target_vor_train, self.alpha_train,
                self.num_at_typ_train, self.at_typ_train], f, protocol=2)
        return None

    def train_mol(self, ml_method, ele):
        '''Train machine learning model for chemical element ele.'''
        size_training = len(self.target_train[ele])
        if len(self.descr_train) == 0:
            logger.error("No molecule in the training set.")
            exit(1)
        self.update_normalizer(ele, predict=False)
        if ml_method == "krr":
            logger.info("building kernel matrix of size (%d,%d); %7.4f Gbytes" \
                % (size_training, size_training, 8*size_training**2/1e9))
            if self.krr_kernel == 'gaussian':
                pairwise_dists = squareform(pdist(self.descr_train[ele], 'euclidean'))/self.normalizer
                bob_dists      = squareform(pdist(self.descr_bob_train[ele], 'cityblock'))/self.normalizer
                baseline_dists = squareform(pdist(self.target_vor_train[ele], 'euclidean'))/self.normalizer
                kmat = scipy.exp(- pairwise_dists**2 / (2.*self.krr_sigma**2)
                    - baseline_dists**2 / (2.*self.krr_zeta**2))
            elif self.krr_kernel == 'laplacian':
                pairwise_dists = squareform(pdist(self.descr_train[ele], 'cityblock'))/self.normalizer
                bob_dists      = squareform(pdist(self.descr_bob_train[ele], 'cityblock'))/self.normalizer
                baseline_dists = squareform(pdist(self.target_vor_train[ele], 'cityblock'))/self.normalizer
                kmat = scipy.exp(- pairwise_dists / self.krr_sigma
                    - bob_dists / self.krr_beta - baseline_dists / self.krr_zeta)
            else:
                print "Kernel",self.krr_kernel,"not implemented."
            kmat += self.krr_lambda*np.identity(len(self.target_train[ele]))
            tgt_prop = [self.target_train[ele][i] - self.target_vor_train[ele][i]
                        for i in xrange(len(self.target_train[ele]))]
            self.alpha_train[ele] = np.linalg.solve(kmat,tgt_prop)
        else:
            logger.error("unknown ML method %s" % ml_method)
            exit(1)
        logger.info("training for %s finished." % ele)
        return None

    def predict_mol(self, _system, charge=0):
        '''Predict multipoles in local reference frame given descriptors.'''
        _system.initialize_multipoles()
        _system.compute_voronoi()
        _system.compute_principal_axes()
        # Rotate the voronoi coefficients
        _system.voronoi_baseline = utils.rotate_mtps( \
            _system.voronoi_baseline, _system.principal_axes)
        _system.build_coulomb_matrices(self.max_neighbors)
        _system.build_bag_of_bonds(self.bob_struct, self.max_neighbors)
        _system.multipoles = np.empty((_system.num_atoms,9))
        # Molecule is prediction set
        self.at_typ_pred = []
        for i in xrange(_system.num_atoms):
            self.at_typ_pred.append(_system.elements[i])
        # Loop over atom types
        for at in constants.atomic_weight.keys():
            if at in self.alpha_train.keys():
                self.update_normalizer(at, predict=True)
                if self.krr_kernel == 'gaussian':
                    pairwise_dists = cdist(_system.coulomb_mat, \
                        self.descr_train[at], 'euclidean')/self.normalizer
                    bob_dists      = cdist(_system.bag_of_bonds, \
                        self.descr_bob_train[at], 'cityblock')/self.normalizer
                    baseline_dists = cdist(_system.voronoi_baseline, \
                        self.target_vor_train[at], 'euclidean')/self.normalizer
                    kmat = scipy.exp(- pairwise_dists**2 / (2.*self.krr_sigma**2)
                        - bob_dists**2 / (2.*self.krr_beta**2)
                        - baseline_dists**2 / (2.*self.krr_zeta**2))
                elif self.krr_kernel == 'laplacian':
                    pairwise_dists = cdist(_system.coulomb_mat, \
                        self.descr_train[at], 'cityblock')/self.normalizer
                    bob_dists      = cdist(_system.bag_of_bonds, \
                        self.descr_bob_train[at], 'cityblock')/self.normalizer
                    baseline_dists = cdist(_system.voronoi_baseline, \
                        self.target_vor_train[at], 'cityblock')/self.normalizer
                    kmat = scipy.exp(- pairwise_dists / self.krr_sigma
                        - bob_dists / self.krr_beta - baseline_dists / self.krr_zeta)
                else:
                    print "Kernel",args.kernel,"not implemented."
                    exit(1)
                pred = np.dot(kmat,self.alpha_train[at])
                for i in xrange(_system.num_atoms):
                    if _system.elements[i] == at:
                        _system.multipoles[i] = pred[i]
        _system.multipoles = utils.rotate_mtps_back(
            _system.multipoles + _system.voronoi_baseline,
            _system.principal_axes)
        # Correct to get integer charge
        if self.calculator.Config.get("multipoles","correct_charge").lower() in \
            ['true','yes','1']:
            totalcharge = sum([mtp[0] for mtp in _system.multipoles])
            abscharge   = sum([abs(mtp[0]) for mtp in _system.multipoles])
            excess_chg = totalcharge - float(charge)
            if abscharge != 0.0:
                for i,mtp_i in enumerate(_system.multipoles):
                    _system.multipoles[i][0] += -1.*excess_chg * \
                        abs(mtp_i[0])/abscharge
        for i in xrange(_system.num_atoms):
            logger.debug("Prediction atom (%s) Q0: %5.2f" \
                % (_system.elements[i],_system.multipoles[i][0]))
            logger.debug("Prediction atom (%s) Q1: (%5.2f, %5.2f, %5.2f)" \
                % (_system.elements[i],_system.multipoles[i][3],
                    _system.multipoles[i][1],_system.multipoles[i][2]))
            logger.debug("Prediction atom (%s) Q2: (%5.2f, %5.2f, %5.2f %5.2f %5.2f)" \
                % (_system.elements[i],_system.multipoles[i][4],
                    _system.multipoles[i][5],_system.multipoles[i][6],
                    _system.multipoles[i][7],_system.multipoles[i][8]))
        logger.debug("Predicted multipoles for %s" % _system)
        return None

    def initialize_bag_of_bonds_struct(self):
        '''Initialize backbone of the bag of bonds descriptor'''
        atoms = constants.bob_atoms
        self.bob_struct = [[ati,atj] for _,ati in enumerate(atoms)
                                     for _,atj in enumerate(atoms)]
        return None

    def update_normalizer(self, atom_type, predict):
        '''Update normalizer for hyperparameters of the machine'''
        x_axis = self.at_typ_train[atom_type]
        y_axis = self.at_typ_train[atom_type]
        if predict:
            x_axis = self.at_typ_pred
        self.normalizer = np.zeros((len(x_axis),len(y_axis)))
        for i in xrange(len(x_axis)):
            for j in xrange(len(y_axis)):
                self.normalizer[i,j] = math.sqrt(self.num_at_typ_train[ \
                    x_axis[i]]*self.num_at_typ_train[y_axis[j]])
                # self.normalizer[i,j] = 1.0
        return None

    def add_mol_to_training(self, new_system, pun, voronoi_pkl=None):
        'Add molecule to training set'
        new_system.initialize_multipoles()
        voronoi_loaded = False
        voronoi_coll = {}
        if voronoi_pkl != None:
            if os.path.exists(voronoi_pkl):
                with open(voronoi_pkl, 'rb') as f:
                    voronoi_coll = pickle.load(f)
                if pun in voronoi_coll.keys():
                    new_system.voronoi_baseline = voronoi_coll[pun]
                    voronoi_loaded = True
        if voronoi_loaded == False:
            new_system.compute_voronoi()
            # Optionally update voronoi_pkl file
            if voronoi_pkl != None:
                voronoi_coll[pun] = new_system.voronoi_baseline
                with open(voronoi_pkl, 'w') as f:
                    pickle.dump(voronoi_coll, f, protocol=2)
        new_system.compute_principal_axes()
        # Rotate the voronoi coefficients
        new_system.voronoi_baseline = utils.rotate_mtps( \
            new_system.voronoi_baseline, new_system.principal_axes)
        new_system.build_coulomb_matrices(self.max_neighbors)
        new_system.build_bag_of_bonds(self.bob_struct, self.max_neighbors)
        new_system.multipoles = np.empty((new_system.num_atoms,9))
        # Read in multipole moments from DMA or hipart txt file
        if pun[-4:] == ".dma":
            new_system.load_mtp_from_dma(pun)
        elif pun[-4:] == ".txt":
            new_system.load_mtp_from_hipart(pun)
        else:
            logger.error("Unrecognized file extension for %s" % pun)
            exit(1)
        for i,ele in enumerate(new_system.elements):
            self.descr_train[ele].append(new_system.coulomb_mat[i])
            self.descr_bob_train[ele].append(new_system.bag_of_bonds[i])
            self.target_vor_train[ele].append(new_system.voronoi_baseline[i])
            self.target_train[ele].append(new_system.multipoles[i])
            self.num_at_typ_train[ele] += 1
            self.at_typ_train[ele].append(ele)
        logger.info("Added file to training set: %s" % new_system)
        return None
