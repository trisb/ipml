This folder contains tarballs to the databases of compounds used in
the machine learning models:
- Hirshfeld ratios
- Atomic density population and width
- Multipole moments and atomic density populations and widths (more
  data than above)

File types for `hirshfeld_database.tgz`:
- xyz: element + coordinates + hirshfeld ratio in the 5th column

File types for `atomic-density_database.tgz`:
- xyz: coordinates
- txt: atomic density population and width (two column file, ordered
  by atoms in the corresponding xyz file)

File types for `multipole-atomicdensity_database.tgz`:
- xyz: coordinates
- dma: GDMA output
- *-mtp.txt: multipole moments output (see Horton documentation)
- *-atmdns.txt: atomic density population and width (two column file,
    ordered by atoms in the corresponding xyz file)
