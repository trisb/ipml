#!/usr/bin/env python
#
# Hydrogen-bond class. Compute hydrogen-bond energy, according to Grimme's QMDFF.
#
# Tristan Bereau (2016)

import numpy as np
import math
from system import System
import logging
import constants

# Set logger
logger = logging.getLogger(__name__)


class Hbond:
    '''
    Hbond class. Compute hydrogen-bond energy, according to Grimme's QMDFF.
    '''

    def __init__(self, sys):
        self.systems = [sys]
        self.atom_in_system = [0]*len(sys.elements)
        logger.setLevel(self.systems[0].get_logger_level())
        self.k_dmp = self.systems[0].Config.getfloat("hbond","k_dmp")
        # Elements involved in donor/acceptor
        self.ele_ad = ['N','O']
        self.k_hbnd_chg, self.k_hbnd_rep = {}, {}
        for ele in self.ele_ad:
            self.k_hbnd_chg[ele] = self.systems[0].Config.getfloat("hbond","k_hbnd_chg["+ele+"]")
            self.k_hbnd_rep[ele] = self.systems[0].Config.getfloat("hbond","k_hbnd_rep["+ele+"]")
        self.energy = 0.0

    def add_system(self, sys):
        self.systems.append(sys)
        last_system_id = self.atom_in_system[-1]
        self.atom_in_system += [last_system_id+1]*len(sys.elements)
        return None

    def include_hbond(self):
        'Include Hbond interaction by tuning point charges'
        # Setup list of atoms to sum over
        atom_coord = [crd for sys in self.systems
                        for _,crd in enumerate(sys.coords)]
        atom_ele = [ele for sys in self.systems
                        for _,ele in enumerate(sys.elements)]
        charges  = [mtp[0] for sys in self.systems
                        for _,mtp in enumerate(sys.multipoles)]
        map_mols = [i for i,sys in enumerate(self.systems)
                       for a in range(sys.num_atoms)]
        map_atoms = [a for i,sys in enumerate(self.systems)
                       for a in range(sys.num_atoms)]
        # if len(charges) == 0:
        #     logger.error("Multipoles not initialized!")
        #     exit(1)
        # i is Acceptor; j is H; k is Donor
        for i,_ in enumerate(atom_coord):
            for j,_ in enumerate(atom_coord):
                for k,_ in enumerate(atom_coord):
                    if (self.different_mols(i,j) and atom_ele[i] in self.ele_ad and
                        atom_ele[j] == "H" and atom_ele[k] in self.ele_ad and
                        not self.different_mols(j,k)):
                        corr = self.hbond_variation(
                            atom_ele[i], atom_coord[i],
                            atom_ele[j], atom_coord[j],
                            atom_ele[k], atom_coord[k])
                        # Correction to point charges
                        # if self.systems[map_mols[k]].multipoles is None:
                        #     raise "Multipoles are not initialized"
                        # chg_corr = corr * \
                        #     self.k_hbnd_chg[atom_ele[i]] * self.k_hbnd_chg[atom_ele[k]]
                        # # Apply +corr to atom k and -corr to atom j. Nothing to i
                        # self.systems[map_mols[k]].multipoles[map_atoms[k]][0] += chg_corr
                        # self.systems[map_mols[j]].multipoles[map_atoms[j]][0] -= chg_corr

                        # Correction to repulsion (scales populations)
                        rep_corr = - corr * self.k_hbnd_rep[atom_ele[k]]
                        # print atom_ele[j],atom_ele[k],rep_corr
                        # Apply +corr to atom j. Nothing to i, k
                        if self.systems[map_mols[j]].valence_widths is None:
                            raise "Valence width is empty"
                        self.systems[map_mols[j]].valence_widths[map_atoms[j]] += rep_corr
        return None

    def different_mols(self, i, j):
        """
        Returns True if atom indices i and j belong to different systems.
        If there's only one system, don't distinguish between different molecules.
        """
        if len(self.systems) == 1:
            return True
        else:
            return self.atom_in_system[i] is not self.atom_in_system[j]

    def hbond_variation(self, ele_i, crd_i, ele_j, crd_j, ele_k, crd_k):
        """
        Returns correction due to H-bonds
        for triplet of atoms i j k, where i is the acceptor,
        j is the hydrogen, and k is the donor. Order AHB.
        """
        # Pairwise vectors
        r_ah = crd_j - crd_i
        r_hb = crd_k - crd_j
        r_ab = crd_k - crd_i
        # Distances
        d_ah = np.linalg.norm(r_ah)
        d_hb = np.linalg.norm(r_hb)
        d_ab = np.linalg.norm(r_ab)
        d_ah2 = d_ah**2
        d_hb2 = d_hb**2
        # Check that H and B are close
        if d_hb > 1.3:
            return 0.0
        # Angle AHB
        cos_ang = np.dot(r_ah,r_ab)/(d_ah*d_ab)
        damp_angle = (0.5*(cos_ang + 1))**6
        # Distance damping terms
        d_cov_ah = 2.*(constants.cov_rad[ele_i] + constants.cov_rad[ele_j])
        damp_dis1 = 1./(1+self.k_dmp*(d_ah/d_cov_ah)**12)
        d_cov_hb = constants.cov_rad[ele_j] + constants.cov_rad[ele_k]
        damp_dis2 = 1./(1+self.k_dmp*(d_hb/d_cov_hb)**12)
        # Prefactor
        # c_hbnd_a = self.k_hbnd[ele_i]
        # c_hbnd_b = self.k_hbnd[ele_k]
        # c_hbnd = c_hbnd_a * c_hbnd_b
        correction = - damp_angle * damp_dis1 * damp_dis2 / d_ab**3
        return correction
