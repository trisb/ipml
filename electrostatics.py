#!/usr/bin/env python
#
# Electrostatics class. Compute multipole electrostatic interactions.
#
# Tristan Bereau (2017)

from system import System
import logging

# Set logger
logger = logging.getLogger(__name__)

class Electrostatics:
    'Electrostatics class. Computes electrostatic interactions.'

    def __init__(self, _calculator):
        self.calculator = _calculator
        logger.setLevel(self.calculator.get_logger_level())
        self.multipoles = None

def mtp_coeff_project(mtp_coeffs, cartesian, atom_types):
    '''Project MTP coefficients (except for Q00) along each atom-atom vector.
    Ordered by atom ID. Will be reshuffled by buildCoulombMatrix().
    Returns mtp_coeffs and principal axes.'''
    retmtp_coeffs = [np.zeros(9)]*len(mtp_coeffs)
    princ_axes = []
    mass = np.zeros(len(atom_types))
    for m in xrange(len(atom_types)):
        mass[m] = atomic_weight[atom_types[m]]
    for i in xrange(len(cartesian)):
        chrg  = mtp_coeffs[i]["Q00"]
        atomi = cartesian[i]
        eigvecs = np.zeros((3,3))
        dip = np.zeros(3)
        if "Q1" in mtp_coeffs[i] or "Q2" in mtp_coeffs[i]:
            # Only one axis defined
            eigvecs[:,0] = cartesian[i]
            for j in xrange(len(cartesian)):
                # Only take neighbor
                if j==i+1:
                    eigvecs[:,0] -= cartesian[j]
            if abs(np.linalg.norm(eigvecs[:,0])) > 1e-12:
                eigvecs[:,0] /= np.linalg.norm(eigvecs[:,0])
            dip = mtp_coeffs[i]["Q1"] * eigvecs[:,0]
            quad = mtp_coeffs[i]["Q2"] * np.outer(eigvecs[:,0],eigvecs[:,0])
            spherQuad = cart_to_spher(quad)
            # Construct other two eigenvectors
            eigvecs[:,1] = np.cross([1,1,1],eigvecs[:,0])
            if abs(np.linalg.norm(eigvecs[:,1])) > 1e-12:
                eigvecs[:,1] /= np.linalg.norm(eigvecs[:,1])
            eigvecs[:,2] = np.cross(eigvecs[:,0],eigvecs[:,1])
            if abs(np.linalg.norm(eigvecs[:,2])) > 1e-12:
                eigvecs[:,2] /= np.linalg.norm(eigvecs[:,2])
            # center of mass of molecule
            com = sum([mass[j]*cartesian[j] for j in \
                xrange(len(cartesian))]) / sum(mass)
            for v in xrange(3):
                if np.dot(com - cartesian[i],eigvecs[v]) < 0.:
                    eigvecs[v] *= -1.
        else:
            spherQuad = np.zeros(5)
            for key,value in mtp_coeffs[i].iteritems():
                if key.startswith('Q1'):
                    dip[map_mtp_coeff[key]] = value
                elif key.startswith('Q2'):
                    spherQuad[map_mtp_coeff[key]] = value
            # quad = spher_to_cart(spherQuad)
            # Compute inertia tensor of molecule around atom i
            coords = cartesian - cartesian[i]
            inertia = np.dot(mass*coords.transpose(),coords)
            eigvals,eigvecs = np.linalg.eig(inertia)
            # center of mass of molecule
            com = sum([mass[j]*cartesian[j] for j in \
                xrange(len(cartesian))]) / sum(mass)
        # Orient first two eigenvectors so that com is in positive quadrant.
        for v in xrange(3):
            if np.dot(com - cartesian[i],eigvecs[v]) < 0.:
                eigvecs[v] *= -1.
        princ_axes.append(eigvecs.transpose())
        retmtp_coeffs[i] = [chrg, dip, spherQuad]
    return retmtp_coeffs, princ_axes
