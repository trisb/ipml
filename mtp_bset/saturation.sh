#!/bin/bash

ncyc=5
nmol=300

for a in H; do #C O N; do
    sata=sat_${a}.dat
    echo $sata
    :> $sata
    for i in 0.01 0.02 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9; do
        save="--save corr_${a}.pkl "
        python train_mtp_ml_bset.py --mols ${nmol} \
            --frac $i --ele $a \
            --cycles $ncyc ${save} > tmp.tmp
        grep Training tmp.tmp > /dev/null
        [[ $? -eq 0 ]] && cat tmp.tmp > corr_${a}.dat
        numtr=$(grep Training tmp.tmp | awk 'NR==1{print $3}')
        mae0=$(grep MAEl0 tmp.tmp | awk '{print $3}')
        mae1=$(grep MAEl1 tmp.tmp | awk '{print $3}')
        mae2=$(grep MAEl2 tmp.tmp | awk '{print $3}')
        echo $numtr $mae0 $mae1 $mae2 >> $sata
    done
done
