#!/usr/bin/env python
#
# Compute interactions for water dimer
#
# Tristan Bereau (2017)

import ipml

from ipml.calculator import Calculator
from ipml.system import System
from ipml.hirshfeld import Hirshfeld
from ipml.induction_calc import InductionCalc
from ipml.multipole_ml_bset import MultipoleMLBSet
from ipml.multipole_calc import MultipoleCalc
from ipml.hbond import Hbond
from ipml.repulsion import Repulsion
from ipml.dispersion import Dispersion
from ipml.cell import Cell
import os, sys, glob, math, pickle
import numpy as np
import glob
import sys
from random import shuffle
import ConfigParser
import operator

np.set_printoptions(precision=4, suppress=True, linewidth=100)

calc = Calculator()
cell = Cell.lattice_parameters(100., 100., 100.)
hirsh = Hirshfeld(calc)
# Load kernel-ridge regression dataset
hirsh.load_ml()
# Load multipoles with aSLATM representation
mtp_ml  = MultipoleMLBSet(calc, descriptor="slatm")
pkls = ["corr_H.pkl", "corr_C.pkl", "corr_O.pkl", "corr_N.pkl"]
for pkl in pkls:
    mtp_ml.load_ml("ipml/mtp_bset_slatm/" + pkl)

def run_calculations(args):
    return compute_system(*args)

def compute_system(filename, ref):
    """Evaluate energies for system described in filename filename"""
    filename = filename[:-4]
    mols = []
    xyzs = []
    for xyz in sorted(glob.glob(filename + "_*.xyz")):
        if xyz[-6:] != "_d.xyz":
            mols.append(System(xyz))
            xyzs.append(xyz)
    num_mols = len(mols)
    hirshc = []
    for mol,xyz in zip(mols,xyzs):
        mtp_ml.predict_mol(mol)
        print mol.multipoles
        hirsh.predict_mol(mol,'krr')
        hirshc = np.append(hirshc, mol.hirshfeld_ratios)
    e_disp = 0.0
    molt = reduce(operator.add, mols)
    hirsh.predict_mol(molt,'krr')
    molt.hirshfeld_ratios = hirshc
    mtp = MultipoleCalc(mols[0], cell)
    rep = Repulsion(mols[0], cell)
    ind = InductionCalc(mols[0], cell)
    hbd = Hbond(mols[0])
    for mol in mols[1:]:
        mtp.add_system(mol)
        rep.add_system(mol)
        ind.add_system(mol)
        hbd.add_system(mol)
    for i,mol in enumerate([molt] + mols):
        fac = 1.0 if i == 0 else -1.0
        # # Compute polarizabilities & MBD
        mbd = Dispersion(mol, cell)
        mbd.compute_csix()
        mbd.compute_freq_scaled_anisotropic()
        try:
            # mbd.short_range_coupling()
            mbd.mbd_protocol()
            e_disp = e_disp + fac*mbd.energy
        except ValueError:
            e_disp += 1.e3
    hbd.include_hbond()
    e_mtp = sum(mtp.mtp_energy())
    e_rep = rep.compute_repulsion("slater_mbis")
    e_ind = ind.polarization_energy()
    etot = e_mtp + e_rep + e_ind + e_disp
    if num_mols == 4:
        etot /= 4.
    print("%-40s %7.3f %7.3f %3.1f" % (filename, etot, float(ref), int(filename[-2:])/10.))
    sys.stdout.flush()
    return list([e_mtp, e_rep, e_ind, e_disp, etot]), ref, float(int(filename[-2:])/10.)

def run_system():
    c = Calculator()
    refs = [-4.320000]
    filenames = ["1161_waterdimer10.xyz"]

    print("# %-38s %7s %7s %s" % ("filename", "Model", "Reference", "Dist.factor"))
    for fil,ref in zip(filenames,refs):
        compute_system(fil,ref)
    sys.stdout.flush()
    return None

def main():
    """Compute interactions for water dimer"""
    run_system()

if __name__ == '__main__':
    main()
