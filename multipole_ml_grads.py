#!/usr/bin/env python
#
# Multipoles_grads class. Predict multipole parameters from ML.
# No local axis system. Instead, rely on derivatives of the Coulomb matrix.
#
# Tristan Bereau (2016)

from electrostatics import Electrostatics
from system import System
import scipy
from scipy import stats
from scipy.spatial.distance import pdist, cdist, squareform
import numpy as np
import logging
import pickle
import constants
import math
import utils
import os

# Set logger
logger = logging.getLogger(__name__)

class MultipoleMLGrads(Electrostatics):
    '''
    MultipoleMLGrads class. Predicts multipoles from machine learning.
    No local axis system. Instead, rely on derivatives of the Coulomb matrix.
    '''

    def __init__(self, _calculator, ml_method="krr"):
        Electrostatics.__init__(self, _calculator)
        self.descr_train = []
        self.target_train = []
        # support vector regression
        self.clf = None
        logger.setLevel(self.calculator.get_logger_level())
        self.max_neighbors = self.calculator.Config.getint( \
            "multipoles","max_neighbors")
        # alpha_train has size 1,3,6
        self.max_coeffs = [1, 3, 6]
        self.alpha_train = None
        self.kernel = self.calculator.Config.get( \
            "multipoles","kernel")
        self.krr_sigma = self.calculator.Config.getfloat( \
            "multipoles","krr_sigma")
        self.krr_lambda = self.calculator.Config.getfloat( \
            "multipoles","krr_lambda")
        self.ml_method = ml_method
        self.num_mols_train = 0

    def load_ml(self, load_file=None):
        '''Load machine learning model'''
        # Try many atoms and see which atoms we find
        if load_file != None:
            logger.info(
                    "Reading multipole training from %s" % load_file)
            with open(load_file, 'rb') as f:
                descr_train_at, alpha_train = pickle.load(f)
                self.descr_train = descr_train_at
                self.alpha_train = alpha_train
        else:
            logger.error("Missing load file name")
            exit(1)
        return None

    def save_ml(self, save_file):
        '''Save machine learning model'''
        logger.info("Saving multipole machine learning model to %s" %
            save_file)
        with open(save_file, 'w') as f:
            pickle.dump([self.descr_train, self.alpha_train], f, protocol=2)
        return None

    def train_mol(self):
        '''Train machine learning model of multipole rank mtp_rank.'''
        size_training = len(self.target_train)
        if len(self.descr_train) == 0:
            logger.error("No molecule in the training set.")
            exit(1)
        logger.info("Training set size: %d atoms; %d molecules" % (size_training,
            self.num_mols_train))
        if self.ml_method == "krr":
            tgt_idx = 0
            self.alpha_train = []
            for rank,coeffs in zip(range(3),[1,3,5]):
                for c in range(coeffs):
                    descr_train_coeff = [self.descr_train[j][rank][c]
                        for j in xrange(len(self.descr_train))]
                    pairwise_dists = squareform(pdist(descr_train_coeff,
                        constants.ml_metric[self.kernel]))
                    tgt_prop = np.array(self.target_train).T[tgt_idx]
                    logger.info("building kernel matrix of size (%d,%d); %7.4f Gbytes" \
                        % (size_training, size_training, 8*size_training**2/1e9))
                    power  = constants.ml_power[self.kernel]
                    prefac = constants.ml_prefactor[self.kernel]
                    kmat = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
                    kmat += self.krr_lambda*np.identity(len(tgt_prop))
                    self.alpha_train.append(np.linalg.solve(kmat,tgt_prop))
                    tgt_idx += 1
        else:
            logger.error("unknown ML method %s" % ml_method)
            exit(1)
        logger.info("training of multipoles finished.")
        return None

    def predict_mol(self, _system, charge=0):
        '''Predict multipoles in local reference frame given descriptors.'''
        _system.initialize_multipoles()
        _system.build_coulomb_grads(self.max_neighbors)
        _system.multipoles = np.zeros((_system.num_atoms,9))
        # Molecule is prediction set
        self.at_typ_pred = []
        for i in xrange(_system.num_atoms):
            self.at_typ_pred.append(_system.elements[i])
        if self.ml_method == "krr":
            tgt_idx = 0
            for rank,coeffs in zip(range(3),[1,3,5]):
                for c in range(coeffs):
                    descr_train_coeff = [self.descr_train[j][rank][c]
                        for j in xrange(len(self.descr_train))]
                    coulmat_c = [_system.coulmat_grads[j][rank][c]
                        for j in range(len(_system.coulmat_grads))]
                    pairwise_dists = cdist(coulmat_c, \
                            descr_train_coeff, constants.ml_metric[self.kernel])
                    power  = constants.ml_power[self.kernel]
                    prefac = constants.ml_prefactor[self.kernel]
                    kmat = scipy.exp(- pairwise_dists**power / (prefac*self.krr_sigma**power))
                    _system.multipoles[:,tgt_idx] = np.dot(kmat,self.alpha_train[tgt_idx])
                    tgt_idx += 1
        else:
            logger.error("unknown ML method %s" % ml_method)
            exit(1)
        # Correct to get integer charge
        if self.calculator.Config.get("multipoles","correct_charge").lower() in \
            ['true','yes','1']:
            totalcharge = sum([mtp[0] for mtp in _system.multipoles])
            abscharge   = sum([abs(mtp[0]) for mtp in _system.multipoles])
            excess_chg = totalcharge - float(charge)
            if abscharge != 0.0:
                for i,mtp_i in enumerate(_system.multipoles):
                    mtp_i[0] += -1.*excess_chg * abs(mtp_i[0])/abscharge
        logger.debug("Predicted multipole expansion for %s" % ( _system))
        return None

    def add_mol_to_training(self, new_system, pun):
        'Add molecule to training set'
        new_system.initialize_multipoles()
        new_system.build_coulomb_grads(self.max_neighbors)
        new_system.multipoles = np.empty((new_system.num_atoms,9))
        # Read in multipole moments from DMA or hipart txt file
        if pun[-4:] == ".dma":
            new_system.load_mtp_from_dma(pun, rotate=False)
        elif pun[-4:] == ".txt":
            new_system.load_mtp_from_hipart(pun, rotate=False)
        else:
            logger.error("Unrecognized file extension for %s" % pun)
            exit(1)
        for i in xrange(len(new_system.elements)):
            self.descr_train.append(new_system.coulmat_grads[i])
            self.target_train.append(new_system.multipoles[i])
        logger.info("Added file to training set: %s" % new_system)
        self.num_mols_train += 1
        return None
