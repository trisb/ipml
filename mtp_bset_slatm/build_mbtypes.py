#!/urs/bin/env python
import qml
import glob
import numpy as np
import pickle
from random import shuffle
from qml.representations import get_slatm_mbtypes

isa_col   = glob.glob(r'../databases/multipole_database/*.xyz')

def build_mbtypes(xyzs):
    qml_mols = []
    for xyz in xyzs:
        mol = qml.Compound(xyz)
        hcon = True
        for i in mol.nuclear_charges:
            if i not in [1,6,7,8]:
                hcon = False
        if hcon:
            qml_mols.append(mol)
    mbtypes = get_slatm_mbtypes(np.array([mol.nuclear_charges
                                               for mol in qml_mols]))
    return mbtypes

print len(isa_col)
mbtypes = None

for n in [1,10,100,200,500,1000,2000,5000,10000]:
    print "%5d" % n,
    shuffle(isa_col)
    mbtypes = build_mbtypes(isa_col[:n])
    print len(mbtypes)

with open("mbtypes.pkl", 'w') as f:
            pickle.dump(mbtypes, f, protocol=2)
