#!/bin/bash

ncyc=1

for a in N C O H; do
    sata=sat_${a}.dat
    echo $sata
    :> $sata
    # for nmol in 10 20 50 100 200 500 1000 2000 5000 10000; do
    for nmol in 10; do
        save="--save corr_${a}.pkl "
        python train_mtp_ml_bset.py --mols ${nmol} \
            --frac 0.7 --ele $a \
            --cycles $ncyc ${save} > tmp.tmp
        [[ $? -ne 0 ]] && break
        grep Training tmp.tmp > /dev/null
        [[ $? -eq 0 ]] && cat tmp.tmp > corr_${a}.dat
        numtr=$(grep Training tmp.tmp | awk 'NR==1{print $3}')
        mae0=$(grep MAEl0 tmp.tmp | awk '{print $3}')
        mae1=$(grep MAEl1 tmp.tmp | awk '{print $3}')
        mae2=$(grep MAEl2 tmp.tmp | awk '{print $3}')
        echo $numtr $mae0 $mae1 $mae2 >> $sata
        echo $numtr $mae0 $mae1 $mae2 
    done
done
